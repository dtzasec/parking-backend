#!/bin/env node

// http://expressjs.com/en/api.html

var express = require('express');
var fs = require('fs');
var http = require('http');
var https = require('https');
var bodyParser = require('body-parser');
var config = require('config');
var ctrl = require('./js/parking-controller.js');
var mongoops = require('./js/mongo-operations.js');

/**
 *  Define the application class
 */
var ParkingBackendService = function() {

    var self = this;


    self.log = function(msg) {
        console.log(new Date().toISOString()+': '+msg);
    };

    /**
     *  Set up server IP address and port # using env variables/defaults.
     */
    self.setupVariables = function() {
        //  Configure environment / application variables

        // Server local IP and listening port
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP || config.get('LISTEN_ADDRESS');
        self.log('LISTEN_ADDRESS: '+self.ipaddress);
        self.port      = process.env.OPENSHIFT_NODEJS_PORT || config.get('LISTEN_PORT');
        self.log('LISTEN_PORT: '+self.port);

        self.pctrl = new ctrl();
    };


    /*  ================================================================  */
    /*  App server functions (main app logic here).                       */
    /*  ================================================================  */

    /********************************************
     * Route processing
     *******************************************/
    self.processStatus = function (req, res) {
        self.log('processStatus: Method = ' + JSON.stringify(req.headers));

        res.setHeader('Content-Type', 'text/plain');
        res.send('Parking Backend: Service is active. Version = ' + config.get('VERSION'));

        self.pctrl.printHello();
    };



    /**
     *  Initialize the server (express) and create the routes and register
     *  the handlers.
     */
    self.initializeServer = function() {
        //self.createRoutes();
        self.app = express();

        // for parsing application/x-www-form-urlencoded
        self.app.use(bodyParser.urlencoded({ extended: false }));

        self.app.get(config.get('URL_PREFIX') + '/status', self.processStatus);
        self.app.get(config.get('URL_PREFIX') + '/getall', self.pctrl.processGetAll);
        self.app.get(config.get('URL_PREFIX') + '/favicon.ico', function(req, res) {
            // Do nothing for now...  return icon in public directory...
        });

        // CRUD interface for the database access, single record functions
        self.app.delete(config.get('URL_PREFIX'), self.pctrl.processDelete);
        self.app.post(config.get('URL_PREFIX'), self.pctrl.processPost);
        self.app.get(config.get('URL_PREFIX'), self.pctrl.processGet);
        self.app.put(config.get('URL_PREFIX'), self.pctrl.processPut);

        /*
        //  Add handlers for the app (from the routes).
        for (var r in self.routes) {
            // GET, POST, PUT, DELETE, etc...
            self.app.all(r, self.routes[r]);
        }
        */
    };

    /**
     *  Initializes the application
     */
    self.initialize = function() {
        console.log("initialize...");
        self.startTimestamp = new Date().toUTCString();
        self.log('*** ParkingBackend Server starting: '+self.startTimestamp+' ***');

        self.setupVariables();

        // Initialize the mongoOps object...
        self.mongoOps = new mongoops();
        self.testMongoOps();

        // Create the express server and routes.
        self.initializeServer();
    };

    self.testMongoOps = function() {
        //self.mongoOps.loadTestData();
        self.mongoOps.getRecordByName("Holiday RV Park", function(rec){
            self.log("testMongoOps: Got record = " + JSON.stringify(rec));
        });
        self.mongoOps.getAllRecords(function(recs){
            self.log("testMongoOps: Got all records = " + JSON.stringify(recs));
        });

    };

    /**
     *  Start the server
     */
    self.start = function() {
        console.log("Listenng on... " + self.port + ", " + self.ipaddress);
        //  start server listener, then update queue
        self.app.listen(self.port, self.ipaddress, function() {
            self.log('Server listening: '+self.ipaddress+':'+self.port);
        });
    };



/*
    self.contactRequestProcessing = function(req, res) {
        try {
            // (agentid, apiuserid, apipasswd, accountnum)

            // Extract URL parameters...
            var fullname = req.query.fullname;
            var accountnum = req.query.acctnum;

            var agentid = self.extractUserID(fullname);

            self.log("contactrequest:  AgentID = " + agentid);
            self.log("contactrequest:  Account num = " + accountnum);

            var strBody = self.ibsaccess1.sendCustomIbsMessage(agentid, accountnum, function (respBody) {
                self.log("contactrequest: Response sent to browser...");
                //self.log("contactrequest: Resp = " + JSON.stringify(respBody));

                res.setHeader('Content-Type', 'text/html');
                res.send(respBody);
            });
        } catch (e) {
            res.setHeader('Content-Type', 'text/html');
            res.send("Five9 WSFS Proxy: 4XX, Missing input parameters.  [error = " + e + "]");
        }
    };
    */
};

/**
 *  main():  Main code.
 */
var pbeservice = new ParkingBackendService();
pbeservice.initialize();
pbeservice.start();

