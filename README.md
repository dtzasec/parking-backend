**ZaSec Parking Locator Service**

CRUD Backend Service for the Parking Location Database

**References:**
https://zellwk.com/blog/crud-express-mongodb/


**Database** 
parkingdb

**Collections:**
spots ==>  parking spot database
users ==>  registered user database

**UserRecord:**
user info: name, address, email
acct info: login name, member level, expiration, last access date
default screen: last address searched
options: 

**SpotRecord**
name
description
rating
address
picture
