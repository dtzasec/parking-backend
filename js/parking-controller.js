/**
 * Created by doug on 11/21/16.
 */
var mongoops = require('./mongo-operations.js');


var ParkingController = function() {
    var self = this;

    self.mongoOps = new mongoops();


    self.log = function(msg) {
        console.log(new Date().toISOString()+': '+msg);
    };

    self.getTitle = function() {
        return 'Parking Controller Title';
    };

    self.printHello = function() {
        self.log('ParkingController: Hello!');
    };

    self.processGet = function (req, res) {
        self.log('ParkingController, processGet: ...');

        var nameVal = req.query.name;
        self.mongoOps.getRecordByName(nameVal, function(err, rec) {
            if (!err) {
                res.setHeader('Content-Type', 'text/plain');
                res.send('Parking Backend: Got the record... ' + JSON.stringify(rec) + '\n');
            } else {
                res.setHeader('Content-Type', 'text/plain');
                res.send('Parking Backend: Record not found!\n');
            }
        });

    };

    self.processPost = function (req, res) {
        self.log('ParkingController, processPost: ...');

        res.setHeader('Content-Type', 'text/plain');
        res.send('Parking Backend: Got the POST...\n');
    };

    self.processPut = function (req, res) {
        self.log('ParkingController, processPut: ...');

        var reqBody = req.body;
        self.log("processPut: Record to add = " + JSON.stringify(reqBody));
        self.mongoOps.addRecord(reqBody);

        res.setHeader('Content-Type', 'text/plain');
        res.send('Parking Backend: Added record to the DB:  ' + JSON.stringify(reqBody) + '\n');
    };

    self.processDelete = function (req, res) {
        self.log('ParkingController, processDelete: ...');

        res.setHeader('Content-Type', 'text/plain');
        res.send('Parking Backend: Got the DELETE...\n');
    };

    self.processGetAll = function (req, res) {
        self.log('ParkingController, processGetAll: ...');

        self.mongoOps.getAllRecords(function (records) {
            res.setHeader('Content-Type', 'application/json');
            res.send(records);
        });
    };
};

module.exports = ParkingController;
