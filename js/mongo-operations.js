/**
 * Created by doug on 11/25/16.
 */
var mongoClient = require("mongodb").MongoClient;
var assert = require('assert');

// The database = parkingdb

var MongoOperations = function () {
    var self = this;
    var url = 'mongodb://localhost:27017/parkingdb';

    // Collection(s)
    var parkingSpotCollection = "spots";

    self.addRecord = function (record) {
        // Use connect method to connect to the server
        mongoClient.connect(url, function(err, db) {
            assert.equal(null, err);
            console.log("MongoOperations, addRecord: Connected correctly to server");

            // Insert a single document
            db.collection(parkingSpotCollection).insertOne(record, function(err, r) {
                assert.equal(null, err);
                assert.equal(1, r.insertedCount);

                console.log("MongoOperations, addRecord: Record added = " + JSON.stringify(record));
                console.log("MongoOperations, addRecord: Number of records added = " + r.insertedCount);
            });

            db.close();
        });
    }

    self.getRecordByName = function (recname, callback) {
        // Use connect method to connect to the server
        mongoClient.connect(url, function(err, db) {
            //assert.equal(null, err);
            console.log("MongoOperations, getRecordByName: Connected successfully to server");

            db.collection(parkingSpotCollection).find({name:recname}).limit(2).toArray(function(err, doc){
                var errVal = false;

                console.log("MongoOperations, getRecordByName: Num records = " + doc.length);
                if (doc.length == 0) {
                    errVal = true;
                }

                callback(errVal, doc);
            });

            db.close();
        });
    }

    self.getAllRecords = function (callback) {
         // Use connect method to connect to the server
        mongoClient.connect(url, function(err, db) {
            //assert.equal(null, err);
            console.log("MongoOperations, getAllRecords: Connected successfully to server");

            db.collection(parkingSpotCollection).find({}).toArray(function(err, docs){
                console.log("MongoOperations, getAllRecords: Num records = " + docs.length);
                callback(docs);
            });

            db.close();
        });
    }

    self.deleteRecordByName = function (recname) {
        // Use connect method to connect to the server
        mongoClient.connect(url, function(err, db) {
            //assert.equal(null, err);
            console.log("MongoOperations, deleteRecord: Connected successfully to server");

            db.collection(parkingSpotCollection).deleteOne({name:recname}, function(err, r) {
                assert.equal(null, err);
                assert.equal(1, r.deletedCount);

                //console.log("MongoOperations, addRecord: Record added = " + JSON.stringify(record));
                console.log("MongoOperations, deleteRecord: Number of records deleted = " + r.deletedCount);
            });

            db.close();
        });
    }

    self.loadTestData = function () {
        var rec1 = {name:"Holiday RV Park",street:"201 Fern Valley Rd",city:"Phoenix",state:"OR",zip:"97535",lat:42.2821083,long:-122.8192354};
        var rec2 = {name:"Pear Tree Motel",street:"300 Pear Tree Ln",city:"Phoenix",state:"OR",zip:"97535",lat:42.2779171,long:-122.8200937};

        self.addRecord(rec1);
        self.addRecord(rec2);
    }
};

module.exports = MongoOperations;
